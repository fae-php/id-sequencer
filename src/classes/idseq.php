<?php

/**
 * FAE 
 */

namespace FAE\idseq;

use FAE\fae\fae;
use FAE\schema\model\model;
use FAE\schema\model\schema;
use Ramsey\Uuid\Codec\OrderedTimeCodec;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class idseq
{

  var $conn;
  var $_rest = false;

  const IDSEQ_TABLE = 'idseq';

  function __construct(?model $instance = null)
  {
    $this->conn = $instance->_conn??null;
  }

  /**
   * Hooked event for all 'set' actions on the model modifying the data array to inject a generated ID
   *
   * @param array $data     Data array being put into the model
   * @param array $filter   Filter criteria that the data are being applied to
   * @param string $type    Insert/update type
   * @param model $instance Original model instance
   * @return array $data    Updated data array
   */
  static function setDataHook(array $data, array $filter, string $type, model $instance)
  {
    // Ignore any action that is not an insert
    if($type != 'insert'){
      return $data;
    }

    // Instantiate a new class of self using model instance for connection data
    $idseq = new self($instance);
    
    if (!array_key_exists('id', $data) || !is_null($data['id'])) {
      if ($id = $idseq->generateId($instance)) {
        $data['id'] = $id;
      }
    } elseif ($type == 'insert' && !is_null($data['id']) && !preg_match('/_revision$/', $instance->_table)) {
      $test = $instance->count(['id' => $data['id']]);
      if ($test > 0) {
        throw new \RuntimeException('ID number given already in use');
      }
    }
    return $data;
  }

  /**
   * Return an auto-generated ID for a column
   *
   * @param model $instance   Instance of the table model
   * @return mixed            An ID appropriate to the column type 
   */
  private function generateId(model $instance)
  {
    $tableSchema = $instance->_models[$instance->_table];
    $idColumnType = $tableSchema->columns->id->type;
    
    switch($idColumnType){
      case 'int':
        return $this->createIntID($instance->_table);
      case 'uuid':
        return Uuid::uuid4();
      case 'ouuid':
        var_dump($tableSchema);
        return $this->generateUuidBinaryOrdered();
    }
  }

  /**
   * Generated a byte ordered UUID string
   * https://www.percona.com/blog/2014/12/19/store-uuid-optimized-way/
   *
   * @return string
   */
  private function generateUuidBinaryOrdered(): string
  {
    $factory = clone Uuid::getFactory();
    $codec = new OrderedTimeCodec($factory->getUuidBuilder());
    $factory->setCodec($codec);
    $uuid = $factory->uuid1();
    return $factory->getCodec()->encodeBinary($uuid);
  }

  /**
   * Create ID sequencer table
   * 
   * @deprecated version 0.2.1
   *
   * @return void
   */
  private function createTable()
  {
    $this->conn->beginTransaction();
    try {
      $schema = new schema($this->dataSource);
      $schema->_models[] = $schema->loadModelFile(self::IDSEQ_TABLE, fae::_path(APPROOT . '/frames/idseq/src/models/idseq.json'));
      $schema->defineSchema();

      $queries = $schema->createSql();
      foreach ($queries as $sql) {
        $this->conn->query($sql);
      }

      $this->conn->commit();
    } catch (\Exception $e) {
      $this->conn->rollback();
      throw $e;
    }
  }

  private function createIntID(string $table): ?int
  {
    $this->conn->beginTransaction();
    try {
      $qb = $this->conn->createQueryBuilder()
        ->select('`nextid`')
        ->from('`' . self::IDSEQ_TABLE . '`')
        ->where('`table` = ?')
        ->setParameter(0, $table);
      $existing = $qb->execute();
      if ($existing->rowCount() != 0) {
        $id = $existing->fetch()['nextid'];
        $this->conn->createQueryBuilder()
          ->update('`' . self::IDSEQ_TABLE . '`')
          ->set('`nextid`', '`nextid`+1')
          ->where('`table` = ?')
          ->setParameter(0, $table)
          ->execute();
      } else {
        $id = 1;
        $insert = $this->conn->createQueryBuilder()
          ->insert('`' . self::IDSEQ_TABLE . '`')
          ->values([
            "`id`" => ":guid",
            "`table`" => ":table",
            "`nextid`" => ":nextid",
          ])
          ->setParameter(":table", $table)
          ->setParameter(":guid", fae::guid())
          ->setParameter(":nextid", $id + 1);
        $insert->execute();
      }
      $this->conn->commit();
    } catch (\Exception $e) {
      $this->conn->rollback();
      throw $e;
      return null;
    }

    return $id;
  }
}
